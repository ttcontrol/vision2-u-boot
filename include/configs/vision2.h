/*
 * Copyright (C) 2007, Guennadi Liakhovetski <lg@denx.de>
 *
 * (C) Copyright 2009 Freescale Semiconductor, Inc.
 *
 * Configuration settings for the MX51-3Stack Freescale board.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CONFIG_H
#define __CONFIG_H

//#define VISION_2_DEVELOPMENT
#undef VISION_2_DEVELOPMENT

#define CONFIG_MX51	/* in a mx51 */
#define CONFIG_SKIP_RELOCATE_UBOOT
#define CONFIG_L2_OFF

#define CONFIG_MX51_HCLK_FREQ		24000000
#define CONFIG_MX51_CLK32			32768
//#define CONFIG_DISPLAY_CPUINFO
//#define CONFIG_DISPLAY_BOARDINFO
#define CONFIG_SYS_NO_FLASH
#define CONFIG_CMD_CACHE

#define GPIO1	0
#define GPIO2	32
#define GPIO3	64
#define GPIO4	96

#define GPIO_SD_CD				(GPIO1+0)
#define GPIO_DAB_PWM 			(GPIO1+2)
#define GPIO_VBAT_EN_KL30		(GPIO1+3)
#define GPIO_WATCHDOG_RESETn 	(GPIO1+4)
#define GPIO_DEBUG_LED			(GPIO1+6)
#define GPIO_12V_SUP_DISn 		(GPIO1+7)
#define GPIO_24MHZ_BOARD_ID		(GPIO1+8)
#define GPIO_KEY_BGLIGHT_EN		(GPIO1+24)
#define GPIO_FEC_RDATA_1		(GPIO2+23)
#define GPIO_FEC_RDATA_2		(GPIO2+27)
#define GPIO_FEC_RDATA_3		(GPIO2+28)
#define GPIO_FEC_RX_ER			(GPIO2+29)
#define GPIO_DAB_DISPLAY_EN		(GPIO3+1)
#define GPIO_WATCHDOG_TRIGGER	(GPIO3+2)
#define GPIO_DISP2_TxEN			(GPIO3+3)
#define GPIO_DAB_BACKLIGHT_EN	(GPIO3+4)
#define GPIO_OUTPORT0			(GPIO3+5)
#define GPIO_OUTPORT1			(GPIO3+6)
#define GPIO_DISP2_GPIO0		(GPIO3+7)
#define GPIO_CAM_EN				(GPIO3+8)
#define GPIO_FEC_COL			(GPIO3+10)
#define GPIO_FEC_RX_CLK			(GPIO3+11)
#define GPIO_OUTPORT2			(GPIO3+12)
#define GPIO_PERIPHERAL_RESET_N	(GPIO3+25)
#define GPIO_BEEPER				(GPIO3+26)
#define GPIO_POWER_OFF			(GPIO3+27)
#define GPIO_FRAM_WE			(GPIO3+30)
#define GPIO_FEC_RDATA_0		(GPIO3+31)
#define GPIO_INPORT0			(GPIO4+21)
#define GPIO_INPORT1			(GPIO4+20)
#define GPIO_INPORT2			(GPIO3+28)
#define GPIO_SPI_CS1n_FLASH		(GPIO4+25)
#define GPIO_EXPANSION_EN		(GPIO4+26)


/*
 * Disabled for now due to build problems under Debian and a significant
 * increase in the final file size: 144260 vs. 109536 Bytes.
 */

#define CONFIG_CMDLINE_TAG	/* enable passing of ATAGs */
#define CONFIG_REVISION_TAG
#define CONFIG_SETUP_MEMORY_TAGS
#define CONFIG_INITRD_TAG
#define CONFIG_VIDEOFB_TAG
#define CONFIG_VISION2_DEVICEINFO_TAG
#define CONFIG_VISION2_BOOTSPLASH_TAG
#define BOARD_LATE_INIT

/*
 * Size of malloc() pool
 */
#define CONFIG_SYS_MALLOC_LEN		(4096 * 1024)

/* size in bytes reserved for initial data */
#define CONFIG_SYS_GBL_DATA_SIZE	128

/*
 * Hardware drivers
 */
#define CONFIG_MXC_UART

/* enable selection of UART used for console output by reading VBAT_EN input pin*/
#define CONFIG_SWITCH_UART_BY_VBAT_EN

#ifndef CONFIG_SWITCH_UART_BY_VBAT_EN
	#define CONFIG_SYS_MX51_UART3
#endif

/*enable Vision� console banner*/
//#define CONFIG_CONSOLE_BANNER


#define CONFIG_MXC_GPIO

#define CONFIG_HW_WATCHDOG

 /*
 * SPI Configs
 * */
#define CONFIG_FSL_SF
#define CONFIG_CMD_SF

#define CONFIG_MXC_SPI
#define CONFIG_SPI_FLASH
#define CONFIG_SPI_FLASH_STMICRO
#define CONFIG_SPI_FLASH_WINBOND
#define CONFIG_SPI_FLASH_EON

/*
 * Use gpio 4 pin 25 as chip select for SPI flash
 * This corresponds to gpio 121
 */
#define CONFIG_SPI_FLASH_CS	     (1 | ( GPIO_SPI_CS1n_FLASH << 8))
#define CONFIG_SF_DEFAULT_MODE   SPI_MODE_0
#define CONFIG_SF_DEFAULT_SPEED  2500000

#define CONFIG_ENV_SPI_CS	    (1 | ( GPIO_SPI_CS1n_FLASH << 8))
#define CONFIG_ENV_SPI_BUS      0
#define CONFIG_ENV_SPI_MAX_HZ	2500000
#define CONFIG_ENV_SPI_MODE	SPI_MODE_0

#define CONFIG_ENV_OFFSET       (63 * 64 * 1024)
#define CONFIG_ENV_SECT_SIZE    (1 * 64 * 1024)
#define CONFIG_ENV_SIZE        	(3 * 1024)

#define CONFIG_FSL_ENV_IN_SF
#define CONFIG_ENV_IS_IN_SPI_FLASH

/* 
 *	SPI boot flash layout:	64 kByte erase-blocksize
 *
 *	0x000000	0x0003FF	1k		empty
 *	0x000400	0x02FFFF	191k	u-boot
 *	0x030000	0x03FFFF	64k		u-boot env
 *	0x040000	0x3FFFFF	3840k	empty
 *
*/

#define CONFIG_FSL_PMIC
#define CONFIG_FSL_PMIC_BUS	0
#define CONFIG_FSL_PMIC_CS	0
#define CONFIG_FSL_PMIC_CLK	2500000
#define CONFIG_FSL_PMIC_MODE	SPI_MODE_0
#define CONFIG_RTC_MC13783

#define CONFIG_CMD_LOADB

/*
 * MMC Configs
 * */
//#define CONFIG_FSL_ESDHC
//#define CONFIG_SYS_FSL_ESDHC_ADDR	(0x70004000)
/* initialize only one SD controller (CTRL2 I/Os are used for SPI) */
//#define CONFIG_SYS_FSL_ESDHC_NUM	1

//#define CONFIG_MMC

//#define CONFIG_CMD_MMC
//#define CONFIG_GENERIC_MMC
//#define CONFIG_CMD_FAT
//#define CONFIG_DOS_PARTITION
//#define CONFIG_CMD_DATE

/*
 * Eth Configs
 */
#define CONFIG_HAS_ETH1
#define CONFIG_NET_MULTI
#define CONFIG_MII
#define CONFIG_DISCOVER_PHY

#define CONFIG_FEC_MXC
#define IMX_FEC_BASE				FEC_BASE_ADDR
#define CONFIG_FEC_MXC_PHYADDR		0x1F

#undef CONFIG_CMD_PING
#define CONFIG_CMD_DHCP
#define CONFIG_CMD_MII
#define CONFIG_CMD_NET

#define CONFIG_CONS_INDEX			3
#define CONFIG_BAUDRATE				115200
#define CONFIG_SYS_BAUDRATE_TABLE	{9600, 19200, 38400, 57600, 115200}

/***********************************************************
 * Command definition
 ***********************************************************/

#include <config_cmd_default.h>

#undef CONFIG_CMD_PING
#define CONFIG_CMD_SPI
#undef CONFIG_CMD_IMLS

#define CONFIG_BOOTDELAY	0
#define CONFIG_ZERO_BOOTDELAY_CHECK

#define CONFIG_PRIME		"FEC0"
#define CONFIG_IPADDR 		10.100.30.130
#define CONFIG_NETMASK 		255.255.0.0
#define CONFIG_ETHADDR          00:50:c2:00:e6:00
#define CONFIG_OVERWRITE_ETHADDR_ONCE
#define CONFIG_SERVERIP 	10.100.10.221
#define CONFIG_LOADADDR		0x90007FC0	/* loadaddr env var */

#define CONFIG_PREBOOT		"ubi part nand0,0;ubi read ${loadaddr} splash; bmp display ${loadaddr} 0x7FFF 0x7FFF; lcdbl on"

//#define VISION_2_DEVELOPMENT
#ifdef VISION_2_DEVELOPMENT

#define	CONFIG_EXTRA_ENV_SETTINGS \
	"netdev=eth0\0" \
	"bootloader.file=u-boot.imx\0" \
	"kernel.file=fImage\0" \
        "splashimg.file=splashimage.bmp\0" \
	"initramfs.file=uInitramfs\0" \
	"partitions=nand0,0\0" \
	"mtdids=nand0=nand\0" \
	"mtdparts=mtdparts=nand:0x1400000(kernel),-(rootfs)\0" \
	"bootloader.update=if tftp ${loadaddr} ${bootloader.file}; then;" \
		"sf probe 0x7901; sf erase 0 0xA0000; sf write ${loadaddr} 0x400 0x${filesize};" \
		"else; echo Bootloader file ${bootloader.file} not found!; fi\0" \
	"bootloader.deleteenv=sf probe 0x7901; sf erase 0x3F0000 0x10000\0" \
        "memtest.run=sf probe 0x7901; sf read 0x1ffe0000 0x380000 0x20000; go 0x1ffe0000\0" \
	"splashimg.show=if ubifsload ${loadaddr} splashimage.bmp; then; bmp display ${loadaddr} 0x7FFF 0x7FFF; lcdbl on; fi\0" \
	"bootargs.base=setenv bootargs quiet lpj=3997696\0" \
	"bootargs.ip.static=setenv bootargs ${bootargs} ip=${ipaddr}::${gatewayip}:${netmask}::${netdev}:off ipdelay=none\0" \
	"bootargs.ip.dhcp=setenv bootargs ${bootargs} ip=:::::${netdev}:dhcp ipdelay=none\0" \
	"bootargs.nfs=setenv bootargs ${bootargs} root=/dev/nfs nfsroot=${serverip}:${nfsroot},v3,tcp\0" \
	"bootargs.ubi=setenv bootargs ${bootargs} root=ubi0:rootfs rootfstype=ubifs ubi.mtd=1\0" \
	"bootcmd.tftp_nfs=run bootargs.base bootargs.ip.static bootargs.nfs; tftpboot ${loadaddr} ${kernel.file}; bootm\0" \
	"bootcmd.tftp_ubi=run bootargs.base bootargs.ip.static bootargs.ubi; tftpboot ${loadaddr} ${kernel.file}; bootm\0" \
	"bootcmd.ubi_nfs=run bootargs.base bootargs.ip.static bootargs.nfs; ubi read ${loadaddr} kernel; bootm\0" \
	"bootcmd.ubi_ubi=run bootargs.base bootargs.ip.static bootargs.ubi; ubi read ${loadaddr} kernel; bootm\0" \
        "bootcmd.initramfs=tftp 0x98000000 ${initramfs.file}; run bootargs.base bootargs.ip.dhcp; tftpboot ${loadaddr} ${kernel.file};" \
		"bootm ${loadaddr} 0x98000000\0" \
	"ethact=FEC\0" 

#else

#define	CONFIG_EXTRA_ENV_SETTINGS \
	"mtdids=nand0=nand\0" \
	"mtdparts=mtdparts=nand:0x1400000(system),-(rootfs)\0" \
        "partitions=nand0,0\0" \
        "mtddevnum=0\0" \
        "mtddevname=system\0" \
        "bootloader.file=vision2/release-current/u-boot.imx\0" \
        "bootloader.update=if tftp ${loadaddr} ${bootloader.file}; then; sf probe 0x7901; sf erase 0 0xA0000; sf write ${loadaddr} 0x400 0x${filesize};else; echo Bootloader file ${bootloader.file} not found!; fi\0" \
        "bootloader.deleteenv=sf probe 0x7901; sf erase 0x3F0000 0x10000\0" \
	"memtest.run=sf probe 0x7901; sf read 0x1ffe0000 0x380000 0x20000; go 0x1ffe0000\0" \
        "bootargs=quiet ipdelay=none root=ubi0:rootfs rootfstype=ubifs ubi.mtd=rootfs lpj=3997696\0" \
	"bootargs.net=setenv bootargs quiet ip=${ipaddr}:::${netmask}::eth0:off ipdelay=none root=ubi0:rootfs rootfstype=ubifs ubi.mtd=rootfs lpj=3997696\0" \
	"verify=n\0" \
	"bootcmd=ubi read ${loadaddr} kernel; bootm\0" \
        "bootcmd.net=run bootargs.net; run bootcmd"

#endif

#if 0
#define CONFIG_EXTRA_ENV_SETTINGS2 \
		"netdev=eth0\0" \
		"kernel=vision2/uImage-vision2.bin\0" \
		"uboot_addr=0xa0000000\0" \
		"uboot=u-boot.bin\0" \
		"partitions=nand0,0\0" \
		"mtdids=nand0=nand\0" \
		"mtdparts=mtdparts=nand:0x1400000(kernel),-(rootfs)\0" \
		"nfspath=/srv/nfs/rootfs/vision2-1\0" \
		"display=cmo\0" \
		"stdin=serial\0" \
		"stdout=serial\0" \
		"stderr=serial\0" \
		"echo_setboot=echo setboot_net; echo setboot_spi; echo setboot_sd; echo setboot_nand; echo\0" \
		"echo_setroot=echo setroot_net; echo setroot_nand; echo setroot_sd;echo\0" \
		"echo_setdisplay=echo setdisplay_7; echo setdisplay_8; echo setdisplay_10; echo\0" \
		"echo_setbase=echo setbase; echo setdefault; echo setlocalip; echo setdefaultip; echo\0" \
		"setboot_net=setenv bootdelay 3; setenv bootcmd run bootcmd_tftp\0" \
		"setboot_spi=setenv bootdelay 1; setenv bootcmd run bootcmd_spi\0" \
		"setboot_sd=setenv bootdelay 3; setenv bootcmd run bootcmd_sd\0" \
		"setboot_nand=setenv bootdelay 1; setenv bootcmd run bootcmd_nand\0" \
		"setroot_nand=setenv ba_root root=ubi0:rootfs rootfstype=ubifs rw\0" \
		"setroot_sd=setenv ba_root root=b302 rw\0" \
		"setroot_net=setenv ba_root root=/dev/nfs nfsroot=${serverip}:${nfspath},v3,tcp\0" \
		"setdisplay_7=setenv ba_display cmo_g070y2_l01_display=1 video=mxcfb:800x480-16@60\0" \
		"setdisplay_8=setenv ba_display nec_nl6448bc26_09c_display=1 video=mxcfb:640x480-16@60\0" \
		"setdisplay_10=setenv ba_display cmo_g104x1_l04_display=1 video=mxcfb:1024x768-16@60\0" \
		"setbase=setenv ba_base console=tty1 console=ttymxc2,${baudrate} consoleblank=0 noinitrd ubi.mtd=1 printk.time=1 ip=${ipaddr}::${gatewayip}:${netmask}::${netdev}:off;\0" \
		"setdefault=sf probe 0x7901; sf erase 0x60000 0x10000; vision2_shutdown\0" \
		"setlocalip=setenv ipaddr 192.168.130.2; setenv serverip 192.168.130.1; setenv netmask 255.255.255.0; setenv gatewayip 192.168.130.1; run setbase\0" \
		"setdefaultip=setenv ipaddr 10.100.30.130; setenv serverip 10.100.10.221; setenv netmask 255.255.0.0; setenv gatewayip 10.100.255.254; run setbase\0" \
		"ba_base=console=tty1 console=ttymxc2,115200 consoleblank=0 noinitrd ubi.mtd=1 printk.time=1 ip=10.100.30.130::10.100.255.254:255.255.0.0::eth0:off;\0" \
		"ba_root=root=b302 rw\0" \
		"ba_display=cmo_g104x1_l04_display=1 video=mxcfb:1024x768-16@60\0" \
		"bootcmd_tftp=setenv bootargs ${ba_base} ${ba_display} ${ba_root}; tftpboot ${loadaddr} ${kernel}; bootm\0" \
		"bootcmd_spi=setenv bootargs ${ba_base} ${ba_display} ${ba_root}; sf probe 0x7901; sf read 0x90800000 0x40000 0x3c0000; bootm\0" \
		"bootcmd_sd=setenv bootargs ${ba_base} ${ba_display} ${ba_root}; mmcinfo; fatload mmc 0:1 ${loadaddr} uimage; bootm\0" \
		"bootcmd_nand=setenv bootargs ${ba_base} ${ba_display} ${ba_root}; nboot nand0,0; bootm\0" \
		"reflash_uboot_hynix=tftp 0x90000000 vision2/u-boot_hynix.imx; sf probe 0x7901; sf erase 0x00000  0x060000; sf write 0x90000000 0x400 0x${filesize}\0" \
		"reflash_uboot_micron=tftp 0x90000000 vision2/u-boot_micron.imx; sf probe 0x7901; sf erase 0x00000  0x060000; sf write 0x90000000 0x400 0x${filesize}\0" \
		"reflash_kernel_spi=tftp 0x90000000 vision2/uImage-vision2.bin; sf probe 0x7901; sf erase 0x70000  0x390000; sf write 0x90000000 0x40000 0x${filesize}\0" \
		"reflash_kernel_nand=tftp ${loadaddr} ${kernel}; nand erase nand0,0; nand write ${loadaddr} nand0,0 0x1400000\0" \
		"bootcmd=run bootcmd_spi\0"
#endif
		
#define CONFIG_ARP_TIMEOUT			200UL

/*
 * Miscellaneous configurable options
 */
#undef CONFIG_SYS_LONGHELP		/* undef to save memory */
#define CONFIG_SYS_PROMPT		"Vision II U-Boot > "
#undef CONFIG_AUTO_COMPLETE
#define CONFIG_SYS_CBSIZE		512	/* Console I/O Buffer Size */
/* Print Buffer Size */
#define CONFIG_SYS_PBSIZE 		(CONFIG_SYS_CBSIZE + sizeof(CONFIG_SYS_PROMPT) + 16)
#define CONFIG_SYS_MAXARGS		64	/* max number of command args */
#define CONFIG_SYS_BARGSIZE 		CONFIG_SYS_CBSIZE /* Boot Argument Buffer Size */

#define CONFIG_SYS_MEMTEST_START 	0x90000000
#define CONFIG_SYS_MEMTEST_END   	0x10000

#define CONFIG_SYS_LOAD_ADDR		CONFIG_LOADADDR

#define CONFIG_SYS_HZ				1000


#ifdef VISION_2_DEVELOPMENT

#define CONFIG_CMDLINE_EDITING

#else

#undef CONFIG_CMDLINE_EDITING

#endif

#define CONFIG_SYS_HUSH_PARSER
#define	CONFIG_SYS_PROMPT_HUSH_PS2	"Vision II U-boot > "

/*-----------------------------------------------------------------------
 * Stack sizes
 *
 * The stack sizes are set up in start.S using the settings below
 */
#define CONFIG_STACKSIZE			(128 * 1024)	/* regular stack */

/*-----------------------------------------------------------------------
 * Physical Memory Map
 */
#define CONFIG_NR_DRAM_BANKS		2

#define PHYS_SDRAM_1				CSD0_BASE_ADDR
#define PHYS_SDRAM_1_SIZE			(256 * 1024 * 1024)
#define PHYS_SDRAM_2				CSD1_BASE_ADDR
#define PHYS_SDRAM_2_SIZE			(256 * 1024 * 1024)

/* Derive DDR clock from axi a (166MHz) (only used if ddr_high_freq_clk_sel not set) */
#define CONFIG_SYS_DDR_CLKSEL		0

/* DDR1 (166MHz)
   Clear ddr_high_freq_clk_sel:
   Derive DDR clock from DDR mux clock source (i.e. axi a for vision2) */
//#define CONFIG_SYS_CLKTL_CBCDR		0x19239100

/* DDR2 (200MHz)
   Set ddr_high_freq_clk_sel:
   Derive DDR clock from PLL1 (i.e. 200MHz for vision2) */
#define CONFIG_SYS_CLKTL_CBCDR		0x59239100

/*
 * Framebuffer and LCD
 */
#define CONFIG_LCD
#define CONFIG_VIDEO_MX5
#define CONFIG_SYS_CONSOLE_ENV_OVERWRITE
#define	CONFIG_SYS_CONSOLE_OVERWRITE_ROUTINE
#define CONFIG_SYS_CONSOLE_IS_IN_ENV
#define LCD_BPP		LCD_COLOR16
#define	CONFIG_SPLASH_SCREEN
#define CONFIG_CMD_BMP
#define CONFIG_BMP_16BPP
#define CMD_DISPLAY
#define CONFIG_SPLASH_SCREEN_ALIGN
#define CONFIG_IPUV3_MAX_CLK		133000000
#define CONFIG_SYS_IPU_HSP_CLKSEL       (1 << 6) /* derive from axi_b */

/*
 * NAND FLASH driver setup
 */
#define CONFIG_CMD_NAND
#define CONFIG_NAND_MX51
#define NAND_MAX_CHIPS        		1
#define CONFIG_SYS_MAX_NAND_DEVICE    	1
#define CONFIG_SYS_NAND_BASE          	0x40000000
#define CONFIG_NAND_FW_16BIT   		0 /* 1: 16bit 0: 8bit */

/*
 * Filesystem
 */
#undef CONFIG_MTD_UBI_DEBUG
#define CONFIG_CMD_UBI
#undef CONFIG_CMD_UBIFS
#define CONFIG_RBTREE
#define CONFIG_MTD_PARTITIONS
#define CONFIG_CMD_MTDPARTS
#define CONFIG_LZO
#define CONFIG_MTD_DEVICE		/* needed for mtdparts commands */

#define CONFIG_CMD_VISION2_SHUTDOWN
#define CONFIG_CMD_VISION2_DEBUG

//#define CONFIG_SILENT_CONSOLE

#endif				/* __CONFIG_H */
