/*
 * (C) Copyright 2010
 * Stefano Babic, DENX Software Engineering, sbabic@denx.de.
 *
 * (C) Copyright 2009 Freescale Semiconductor, Inc.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <asm/io.h>
#include <asm/arch/imx-regs.h>
#include <asm/arch/mx51_pins.h>
#include <asm/arch/crm_regs.h>
#include <asm/arch/iomux.h>
#include <mxc_gpio.h>
#include <asm/arch/sys_proto.h>
#include <asm/errno.h>
#include <i2c.h>
#include <mmc.h>
#include <fsl_esdhc.h>
#include <fsl_pmic.h>
#include <mc13892.h>
#include <linux/fb.h>
#include <asm/cache.h>


DECLARE_GLOBAL_DATA_PTR;

static u32 system_rev;

extern int mx51_fb_init(struct fb_videomode *mode);

#ifdef CONFIG_HW_WATCHDOG
#include <watchdog.h>

struct fb_videomode videomode;

void hw_watchdog_reset(void)
{
	int val;
	
	/* toggle watchdog trigger pin */
	val = mxc_gpio_get(GPIO_WATCHDOG_TRIGGER);
	val = val ? 0 : 1;
	mxc_gpio_set(GPIO_WATCHDOG_TRIGGER, val);
}
#endif

static void init_drive_strength(void)
{


//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_PKEADDR, PAD_CTL_PKE_NONE);			// is set in .cfg file
	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DDRPKS, 0); ///

	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_SDCKE0,	0xe5); ///
	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_SDCKE1,	0xe5); ///




//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DDR_SR_A0, PAD_CTL_SRE_SLOW);		// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DDR_SR_A1, PAD_CTL_SRE_SLOW);		// is set in .cfg file

//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DDR_SR_B0, PAD_CTL_SRE_SLOW);		// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DDR_SR_B1, PAD_CTL_SRE_SLOW);		// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DDR_SR_B2, PAD_CTL_SRE_SLOW);		// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DDR_SR_B4, PAD_CTL_SRE_SLOW);		// is set in .cfg file

///	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_HYSDDR0, PAD_CTL_HYS_ENABLE);
///	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_HYSDDR1, PAD_CTL_HYS_ENABLE);
///	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_HYSDDR2, PAD_CTL_HYS_ENABLE);
///	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_HYSDDR3, PAD_CTL_HYS_ENABLE);

//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DDRAPKS, PAD_CTL_PUE_KEEPER); 	// irrelevant because is off
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DDRAPUS, PAD_CTL_100K_PD);		// irrelevant because is off
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DDR_A0, PAD_CTL_DRV_MEDIUM);		// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DDR_A1, PAD_CTL_DRV_MEDIUM);		// is set in .cfg file
	
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_INMODE1, PAD_CTL_DDR_INPUT_CMOS);// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_PKEDDR, PAD_CTL_PKE_ENABLE);		//default
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DDRPUS, PAD_CTL_100K_PU);		//default
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DRAM_B0, PAD_CTL_DRV_HIGH);		// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DRAM_B1, PAD_CTL_DRV_HIGH);		// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DRAM_B2, PAD_CTL_DRV_HIGH);		// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_GRP_DRAM_B4, PAD_CTL_DRV_HIGH);		// is set in .cfg file

//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_RAS, PAD_CTL_DRV_HIGH | PAD_CTL_SRE_SLOW); //use medium at DDR2 without termination
//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_CAS, PAD_CTL_DRV_MAX | PAD_CTL_SRE_SLOW); //use medium at DDR2 without termination
//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_SDWE, PAD_CTL_PKE_NONE | PAD_CTL_DRV_HIGH | PAD_CTL_SRE_FAST); //use medium at DDR2 without termination
	
//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_SDCLK, PAD_CTL_PKE_NONE | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST);		// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_SDQS0, PAD_CTL_HYS_NONE | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL | PAD_CTL_100K_PD | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST);		// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_SDQS1, PAD_CTL_HYS_NONE | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL | PAD_CTL_100K_PD | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST);		// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_SDQS2, PAD_CTL_HYS_NONE | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL | PAD_CTL_100K_PD | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST);		// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_SDQS3, PAD_CTL_HYS_NONE | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL | PAD_CTL_100K_PD | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST);		// is set in .cfg file

//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_CS0, PAD_CTL_PKE_NONE | PAD_CTL_DRV_HIGH | PAD_CTL_SRE_FAST);	// is set in .cfg file
//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_CS1, PAD_CTL_PKE_NONE | PAD_CTL_DRV_HIGH | PAD_CTL_SRE_FAST);	// is set in .cfg file

//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_DQM0, PAD_CTL_PKE_NONE | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST); // single bank
//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_DQM1, PAD_CTL_PKE_NONE | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST); // single bank
//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_DQM2, PAD_CTL_PKE_NONE | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST); // single bank
//	mxc_iomux_set_pad(MX51_PIN_CTL_DRAM_DQM3, PAD_CTL_PKE_NONE | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST); // single bank

	
}

u32 get_board_rev(void)
{
	system_rev = get_cpu_rev();

	return system_rev;
}

int dram_init(void)
{
	/* Check RAM size pin */
	mxc_request_iomux(MX51_PIN_GPIO1_8 , IOMUX_CONFIG_ALT0); /* GPIO1_8 */
	mxc_iomux_set_pad(MX51_PIN_GPIO1_8 , 0x00);
	mxc_gpio_direction(GPIO_24MHZ_BOARD_ID, MXC_GPIO_DIRECTION_IN);

	gd->bd->bi_dram[0].start = PHYS_SDRAM_1;
	gd->bd->bi_dram[0].size = get_ram_size((long *)PHYS_SDRAM_1, PHYS_SDRAM_1_SIZE);

	if (mxc_gpio_get(GPIO_24MHZ_BOARD_ID)) /* 2 (pin low) or 4 (pin high) DDR chips populated ? */
	{
		gd->bd->bi_dram[1].start = PHYS_SDRAM_2;
		gd->bd->bi_dram[1].size = get_ram_size((long *)PHYS_SDRAM_2, PHYS_SDRAM_2_SIZE);
	}
	else
	{
		gd->bd->bi_dram[1].start = 0;
		gd->bd->bi_dram[1].size = 0;
	}

	mxc_iomux_set_pad(MX51_PIN_GPIO1_8 , 0xc5); /* back to default */
	mxc_free_iomux(MX51_PIN_GPIO1_8, IOMUX_CONFIG_ALT0); /* GPIO1_8 */

	return 0;
}

static void setup_weim(void)
{
	struct weim  *pweim = (struct weim *)WEIM_BASE_ADDR;

	pweim->csgcr1 = 0x004100b9;
	pweim->csgcr2 = 0x00000001;
	pweim->csrcr1 = 0x0a018000;
	pweim->csrcr2 = 0;
	pweim->cswcr1 = 0x0704a240;
}

static void setup_uart(void)
{
	unsigned int pad = PAD_CTL_HYS_ENABLE | PAD_CTL_PKE_ENABLE | PAD_CTL_PUE_PULL | PAD_CTL_DRV_HIGH | PAD_CTL_SRE_FAST;

#ifdef CONFIG_SWITCH_UART_BY_VBAT_EN	
	/* Set UART depending on VBAT_EN pin */
	if (!(mxc_gpio_get(GPIO_VBAT_EN_KL30)))
	{
		/*set the base address for the corresponding uart*/
		gd->uart_base_address = 0x07000C000; // UART3_BASE_ADDR;
		
#endif
  	/* console RX on Pin EIM_D25 */
  	mxc_request_iomux(MX51_PIN_EIM_D25, IOMUX_CONFIG_ALT3);
  	mxc_iomux_set_pad(MX51_PIN_EIM_D25, pad);
  	/* console TX on Pin EIM_D26 */
  	mxc_request_iomux(MX51_PIN_EIM_D26, IOMUX_CONFIG_ALT3);
  	mxc_iomux_set_pad(MX51_PIN_EIM_D26, pad);
#ifdef CONFIG_SWITCH_UART_BY_VBAT_EN	
	}
	else
	{
		/*set the base address for the corresponding uart*/
		gd->uart_base_address = 0x073FBC000;//UART1_BASE_ADDR;
		
		/* console RX on Pin EIM_D25 */
  	mxc_request_iomux(MX51_PIN_UART1_RXD, IOMUX_CONFIG_ALT0);
  	mxc_iomux_set_pad(MX51_PIN_UART1_RXD, pad);
  	/* console TX on Pin EIM_D26 */
  	mxc_request_iomux(MX51_PIN_UART1_TXD, IOMUX_CONFIG_ALT0);
  	mxc_iomux_set_pad(MX51_PIN_UART1_TXD, pad);
	}	
#endif
}

#ifdef CONFIG_MXC_SPI
void spi_io_init(void)
{
	/* 000: Select mux mode: ALT0 mux port: MOSI of instance: ecspi1 */
	mxc_request_iomux(MX51_PIN_CSPI1_MOSI, IOMUX_CONFIG_ALT0);
	mxc_iomux_set_pad(MX51_PIN_CSPI1_MOSI, PAD_CTL_HYS_ENABLE | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST);

	/* 000: Select mux mode: ALT0 mux port: MISO of instance: ecspi1. */
	mxc_request_iomux(MX51_PIN_CSPI1_MISO, IOMUX_CONFIG_ALT0);
	mxc_iomux_set_pad(MX51_PIN_CSPI1_MISO, PAD_CTL_HYS_ENABLE | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST);

	/* 000: Select mux mode: ALT0 mux port: SS0 of instance: ecspi1. */
	mxc_request_iomux(MX51_PIN_CSPI1_SS0, IOMUX_CONFIG_ALT0);
	mxc_iomux_set_pad(MX51_PIN_CSPI1_SS0, PAD_CTL_HYS_ENABLE | PAD_CTL_PKE_ENABLE | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST);

	/* SS1 will be used as GPIO because of uninterrupted long SPI transmissions (GPIO4_25)*/
	mxc_request_iomux(MX51_PIN_CSPI1_SS1, IOMUX_CONFIG_ALT3);
	mxc_iomux_set_pad(MX51_PIN_CSPI1_SS1, PAD_CTL_HYS_ENABLE | PAD_CTL_PKE_ENABLE | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST);

	/* 000: Select mux mode: ALT0 mux port: SS2 of instance: ecspi1. */
	mxc_request_iomux(MX51_PIN_DI1_PIN11, IOMUX_CONFIG_ALT7);
	mxc_iomux_set_pad(MX51_PIN_DI1_PIN11, PAD_CTL_HYS_ENABLE | PAD_CTL_PKE_ENABLE | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST);

	/* 000: Select mux mode: ALT0 mux port: SCLK of instance: ecspi1. */
	mxc_request_iomux(MX51_PIN_CSPI1_SCLK, IOMUX_CONFIG_ALT0);
	mxc_iomux_set_pad(MX51_PIN_CSPI1_SCLK, PAD_CTL_HYS_ENABLE | PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST);

}

static void reset_peripherals(int reset)
{
	if (reset) {
		/* prepare reset_n pin */
		/* reset_n is on NANDF_D15 */
		mxc_gpio_set(GPIO_PERIPHERAL_RESET_N, 0);
		mxc_gpio_direction(GPIO_PERIPHERAL_RESET_N, MXC_GPIO_DIRECTION_OUT);

		mxc_gpio_set(GPIO_FEC_RX_CLK, 0);
		mxc_gpio_direction(GPIO_FEC_RX_CLK, MXC_GPIO_DIRECTION_OUT);
		mxc_gpio_set(GPIO_FEC_COL, 1);
		mxc_gpio_direction(GPIO_FEC_COL, MXC_GPIO_DIRECTION_OUT);
		mxc_gpio_set(GPIO_FEC_RDATA_0, 1);
		mxc_gpio_direction(GPIO_FEC_RDATA_0, MXC_GPIO_DIRECTION_OUT);

		/* set direction of FEC config lines */
		mxc_gpio_set(GPIO_FEC_RDATA_2, 0);
		mxc_gpio_direction(GPIO_FEC_RDATA_2, MXC_GPIO_DIRECTION_OUT);
		mxc_gpio_set(GPIO_FEC_RDATA_3, 0);
		mxc_gpio_direction(GPIO_FEC_RDATA_3, MXC_GPIO_DIRECTION_OUT);
		mxc_gpio_set(GPIO_FEC_RX_ER, 0);
		mxc_gpio_direction(GPIO_FEC_RX_ER, MXC_GPIO_DIRECTION_OUT);
		mxc_gpio_set(GPIO_FEC_RDATA_1, 1);
		mxc_gpio_direction(GPIO_FEC_RDATA_1, MXC_GPIO_DIRECTION_OUT);

		/* FEC_RXD1 - sel GPIO (2-23) for configuration -> 1 */
		mxc_request_iomux(MX51_PIN_EIM_EB3, IOMUX_CONFIG_ALT1);
		/* FEC_RXD2 - sel GPIO (2-27) for configuration -> 0 */
		mxc_request_iomux(MX51_PIN_EIM_CS2, IOMUX_CONFIG_ALT1);
		/* FEC_RXD3 - sel GPIO (2-28) for configuration -> 0 */
		mxc_request_iomux(MX51_PIN_EIM_CS3, IOMUX_CONFIG_ALT1);
		/* FEC_RXER - sel GPIO (2-29) for configuration -> 0 */
		mxc_request_iomux(MX51_PIN_EIM_CS4, IOMUX_CONFIG_ALT1);
		/* FEC_COL  - sel GPIO (3-10) for configuration -> 1 */
		mxc_request_iomux(MX51_PIN_NANDF_RB2, IOMUX_CONFIG_ALT3);
		/* FEC_RCLK - sel GPIO (3-11) for configuration -> 0 */
		mxc_request_iomux(MX51_PIN_NANDF_RB3, IOMUX_CONFIG_ALT3);
		/* FEC_RXD0 - sel GPIO (3-31) for configuration -> 1 */	
		mxc_request_iomux(MX51_PIN_NANDF_D9, IOMUX_CONFIG_ALT3);
		/*
		 * activate reset_n pin
		 * Select mux mode: ALT3 mux port: NAND D15 
		 */
		mxc_request_iomux(MX51_PIN_NANDF_D15, IOMUX_CONFIG_ALT3);
		mxc_iomux_set_pad(MX51_PIN_NANDF_D15, PAD_CTL_DRV_HIGH | PAD_CTL_22K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_HYS_ENABLE | PAD_CTL_DRV_VOT_HIGH );
	} else {
		/* set FEC Control lines */
		mxc_gpio_direction(GPIO_PERIPHERAL_RESET_N, MXC_GPIO_DIRECTION_IN);
		udelay(500);
		/* FEC RDATA[3] */
		mxc_request_iomux(MX51_PIN_EIM_CS3, IOMUX_CONFIG_ALT3);
		mxc_iomux_set_pad(MX51_PIN_EIM_CS3, 0x180);

		/* FEC RDATA[2] */
		mxc_request_iomux(MX51_PIN_EIM_CS2, IOMUX_CONFIG_ALT3);
		mxc_iomux_set_pad(MX51_PIN_EIM_CS2, 0x180);

		/* FEC RDATA[1] */
		mxc_request_iomux(MX51_PIN_EIM_EB3, IOMUX_CONFIG_ALT3);
		mxc_iomux_set_pad(MX51_PIN_EIM_EB3, 0x180);

		/* FEC RDATA[0] */
		mxc_request_iomux(MX51_PIN_NANDF_D9, IOMUX_CONFIG_ALT2);
		mxc_iomux_set_pad(MX51_PIN_NANDF_D9, 0x2180);

		/* FEC RX_CLK */
		mxc_request_iomux(MX51_PIN_NANDF_RB3, IOMUX_CONFIG_ALT1);
		mxc_iomux_set_pad(MX51_PIN_NANDF_RB3, 0x2180);

		/* FEC RX_ER */
		mxc_request_iomux(MX51_PIN_EIM_CS4, IOMUX_CONFIG_ALT3);
		mxc_iomux_set_pad(MX51_PIN_EIM_CS4, 0x180);

		/* FEC COL */
		mxc_request_iomux(MX51_PIN_NANDF_RB2, IOMUX_CONFIG_ALT1);
		mxc_iomux_set_pad(MX51_PIN_NANDF_RB2, 0x2180);
	}
}

static void power_init_mx51(void)
{
	unsigned int val;

	/* Write needed to Power Gate 2 register */
	val = pmic_reg_read(REG_POWER_MISC);

	/* Set core voltage (SW1) to 1.1V */
	val = pmic_reg_read(24);
	val &= ~0x1F;
	val |= 0x14;
	pmic_reg_write(24, val);

	/* Setup VCC (SW2) to 1.225 (TO 3.0) */
	val = pmic_reg_read(25);
	val &= ~0x1F;
	val |= 0x19;
	pmic_reg_write(25, val);

	/* Setup SW3 to 1.225 (TO 3.0) */
	val = pmic_reg_read(26);
	val &= ~0x1F;
	val |= 0x19;
	pmic_reg_write(26, val);

	/* Setup SW4 to 1.85 (TO 3.0) */
	val = pmic_reg_read(27);
	val &= ~0x1F;
	val |= 0x1F;
	pmic_reg_write(27, val);

	/* Setup SW frequ to max */
	val = pmic_reg_read(28);
	val &= ~0x380000;
	val |= 0x380000;
	pmic_reg_write(28, val);
	
	/* Set switchers in Auto in NORMAL mode & STANDBY mode */
	/* Setup the switcher mode for SW1 & SW2*/
	val = pmic_reg_read(REG_SW_4);
	val = (val & ~((SWMODE_MASK << SWMODE1_SHIFT) |
		(SWMODE_MASK << SWMODE2_SHIFT)));
	val |= (SWMODE_AUTO_AUTO << SWMODE1_SHIFT) |
		(SWMODE_AUTO_AUTO << SWMODE2_SHIFT);
	pmic_reg_write(REG_SW_4, val);

	/* Setup the switcher mode for SW3 & SW4 */
	val = pmic_reg_read(REG_SW_5);
	val &= ~((SWMODE_MASK << SWMODE4_SHIFT) |
		(SWMODE_MASK << SWMODE3_SHIFT));
	val |= (SWMODE_AUTO_AUTO << SWMODE4_SHIFT) |
		(SWMODE_AUTO_AUTO << SWMODE3_SHIFT);
	pmic_reg_write(REG_SW_5, val);

	/* Power limiter disable (PLIMDIS) */
	val = pmic_reg_read(48);
	val |= 0x200;
	pmic_reg_write(48, val);

	/* Switcher current limit disable (SWILIMB), Erratum nr. 8690 */
	val = pmic_reg_read(28);
	val |= 0x400000;
	pmic_reg_write(28, val);

	/* Set VGEN3 to 1.8V, VCAM to 3.0V */
	val = pmic_reg_read(REG_SETTING_0);
	val &= ~(VCAM_MASK | VGEN3_MASK);
	val |= VCAM_3_0;
	pmic_reg_write(REG_SETTING_0, val);

	/* Set VVIDEO to 2.775V, VAUDIO to 3V0, VSD to 1.8V */
	val = pmic_reg_read(REG_SETTING_1);
	val &= ~(VVIDEO_MASK | VSD_MASK | VAUDIO_MASK);
	val |= VVIDEO_2_775 | VAUDIO_3_0 | VSD_1_8;
	pmic_reg_write(REG_SETTING_1, val);

	udelay(200);

	reset_peripherals(1);

	/* Enable VGEN3, VCAM, VAUDIO, VVIDEO, VSD regulators */
	val = VGEN3EN | VGEN3CONFIG | VCAMEN | VCAMCONFIG | VCAMSTBY |
		VVIDEOEN | VAUDIOEN  | VSDEN;
	pmic_reg_write(REG_MODE_1, val);

	val = pmic_reg_read(REG_POWER_CTL2);
	val |= WDIRESET;
	pmic_reg_write(REG_POWER_CTL2, val);

	udelay(2500);
	reset_peripherals(0);
}
#endif

static void setup_gpios(void)
{
/* 12V_SUP_DISn */
	mxc_request_iomux(MX51_PIN_GPIO1_7 , IOMUX_CONFIG_ALT0);
	mxc_iomux_set_pad(MX51_PIN_GPIO1_7 , 0x82);
	mxc_gpio_set(GPIO_12V_SUP_DISn, 1); // set to on
	mxc_gpio_direction(GPIO_12V_SUP_DISn, MXC_GPIO_DIRECTION_OUT);


/* config CAM_EN */
	mxc_request_iomux(MX51_PIN_DISPB2_SER_RS , IOMUX_CONFIG_ALT4); /* GPIO3_8 */
	mxc_iomux_set_pad(MX51_PIN_DISPB2_SER_RS , 0x82);
	mxc_gpio_set(GPIO_CAM_EN, 0); // set to off
	mxc_gpio_direction(GPIO_CAM_EN, MXC_GPIO_DIRECTION_OUT);
	

/* config EXT_VBAT_EN_KL30 */
	mxc_request_iomux(MX51_PIN_GPIO1_3 , IOMUX_CONFIG_ALT0);
	mxc_iomux_set_pad(MX51_PIN_GPIO1_3 , 0x100);
	mxc_gpio_direction(GPIO_VBAT_EN_KL30, MXC_GPIO_DIRECTION_IN);
	if (mxc_gpio_get(GPIO_VBAT_EN_KL30))
	{
		gd->flags |= GD_FLG_VBAT_EN;
	}


// Set GPIO1_4 to high and output; it is used to reset the system on reboot
	mxc_request_iomux(MX51_PIN_GPIO1_4 , IOMUX_CONFIG_ALT0);
	mxc_iomux_set_pad(MX51_PIN_GPIO1_4 , 0xe2);
	mxc_gpio_set(GPIO_WATCHDOG_RESETn, 1);
	mxc_gpio_direction(GPIO_WATCHDOG_RESETn, MXC_GPIO_DIRECTION_OUT);


/* DAB Display EN */
	mxc_request_iomux(MX51_PIN_DI1_PIN12 , IOMUX_CONFIG_ALT4); /* GPIO3_1 */
	mxc_iomux_set_pad(MX51_PIN_DI1_PIN12 , 0x82);
	mxc_gpio_set(GPIO_DAB_DISPLAY_EN, 1);
	mxc_gpio_direction(GPIO_DAB_DISPLAY_EN, MXC_GPIO_DIRECTION_OUT);

/* WDOG_TRIGGER */
	mxc_request_iomux(MX51_PIN_DI1_PIN13 , IOMUX_CONFIG_ALT4); /* GPIO3_2 */
	mxc_iomux_set_pad(MX51_PIN_DI1_PIN13 , 0x82);
	mxc_gpio_set(GPIO_WATCHDOG_TRIGGER, 0);
	mxc_gpio_direction(GPIO_WATCHDOG_TRIGGER, MXC_GPIO_DIRECTION_OUT);

	/* Now we need to trigger the watchdog */
	WATCHDOG_RESET();


/* Display2 TxEN */
	mxc_request_iomux(MX51_PIN_DI1_D0_CS , IOMUX_CONFIG_ALT4); /* GPIO3_3 */
	mxc_iomux_set_pad(MX51_PIN_DI1_D0_CS , 0x82);
	mxc_gpio_set(GPIO_DISP2_TxEN, 0);
	mxc_gpio_direction(GPIO_DISP2_TxEN, MXC_GPIO_DIRECTION_OUT);


/* DAB Backlight EN */
	mxc_request_iomux(MX51_PIN_DI1_D1_CS , IOMUX_CONFIG_ALT4); /* GPIO3_4 */
	mxc_iomux_set_pad(MX51_PIN_DI1_D1_CS , 0x82);
	mxc_gpio_set(GPIO_DAB_BACKLIGHT_EN, 0);
	mxc_gpio_direction(GPIO_DAB_BACKLIGHT_EN, MXC_GPIO_DIRECTION_OUT);


/* OUTPORT 0 */
	mxc_request_iomux(MX51_PIN_DISPB2_SER_DIN , IOMUX_CONFIG_ALT4); /* GPIO3_5 */
	mxc_iomux_set_pad(MX51_PIN_DISPB2_SER_DIN , 0x82);
	mxc_gpio_set(GPIO_OUTPORT0, 0);
	mxc_gpio_direction(GPIO_OUTPORT0, MXC_GPIO_DIRECTION_OUT);


/* OUTPORT 1 */
	mxc_request_iomux(MX51_PIN_DISPB2_SER_DIO , IOMUX_CONFIG_ALT4); /* GPIO3_6 */
	mxc_iomux_set_pad(MX51_PIN_DISPB2_SER_DIO , 0x82);
	mxc_gpio_set(GPIO_OUTPORT1, 0);
	mxc_gpio_direction(GPIO_OUTPORT1, MXC_GPIO_DIRECTION_OUT);


/* OUTPORT 2 */
	mxc_request_iomux(MX51_PIN_GPIO_NAND , IOMUX_CONFIG_ALT0); /* GPIO3_12 */
	mxc_iomux_set_pad(MX51_PIN_GPIO_NAND , 0x82);
	mxc_gpio_set(GPIO_OUTPORT2, 0);
	mxc_gpio_direction(GPIO_OUTPORT2, MXC_GPIO_DIRECTION_OUT);


/* INPORT 0 */
	mxc_request_iomux(MX51_PIN_AUD3_BB_FS , IOMUX_CONFIG_ALT3);
	mxc_iomux_set_pad(MX51_PIN_AUD3_BB_FS , PAD_CTL_PKE_NONE | PAD_CTL_HYS_ENABLE);
	mxc_gpio_direction(GPIO_INPORT0, MXC_GPIO_DIRECTION_IN);
	
	
/* INPORT 1 */
	mxc_request_iomux(MX51_PIN_AUD3_BB_CK , IOMUX_CONFIG_ALT3);
	mxc_iomux_set_pad(MX51_PIN_AUD3_BB_CK , PAD_CTL_PKE_NONE | PAD_CTL_HYS_ENABLE);
	mxc_gpio_direction(GPIO_INPORT1, MXC_GPIO_DIRECTION_IN);
	
	
/* INPORT 2 */
	mxc_request_iomux(MX51_PIN_NANDF_D12 , IOMUX_CONFIG_ALT3);
	mxc_iomux_set_pad(MX51_PIN_NANDF_D12 , PAD_CTL_PKE_NONE | PAD_CTL_HYS_ENABLE);
	mxc_gpio_direction(GPIO_INPORT2, MXC_GPIO_DIRECTION_IN);


/* DISP2_GPIO */
	mxc_request_iomux(MX51_PIN_DISPB2_SER_CLK , IOMUX_CONFIG_ALT4); /* GPIO3_7 */
	mxc_iomux_set_pad(MX51_PIN_DISPB2_SER_CLK , 0x82);
	mxc_gpio_set(GPIO_DISP2_GPIO0, 0);
	mxc_gpio_direction(GPIO_DISP2_GPIO0, MXC_GPIO_DIRECTION_OUT);


/* FRAM_WE */
	mxc_request_iomux(MX51_PIN_NANDF_D10 , IOMUX_CONFIG_ALT3); /* GPIO3_30 */
	mxc_iomux_set_pad(MX51_PIN_NANDF_D10 , PAD_CTL_DRV_HIGH | PAD_CTL_DRV_VOT_HIGH);
	mxc_gpio_set(GPIO_FRAM_WE, 0);
	mxc_gpio_direction(GPIO_FRAM_WE, MXC_GPIO_DIRECTION_OUT);


/* BEEPER_EN */
	mxc_request_iomux(MX51_PIN_NANDF_D14 , IOMUX_CONFIG_ALT3); /* GPIO3_26 */
	mxc_iomux_set_pad(MX51_PIN_NANDF_D14 , PAD_CTL_DRV_HIGH | PAD_CTL_DRV_VOT_HIGH);
	mxc_gpio_set(GPIO_BEEPER, 0);
	mxc_gpio_direction(GPIO_BEEPER, MXC_GPIO_DIRECTION_OUT);


/* POWER_OFF */
	mxc_request_iomux(MX51_PIN_NANDF_D13 , IOMUX_CONFIG_ALT3); /* GPIO3_27 */
	mxc_iomux_set_pad(MX51_PIN_NANDF_D13 , PAD_CTL_DRV_HIGH | PAD_CTL_DRV_VOT_HIGH);
	mxc_gpio_set(GPIO_POWER_OFF, 1); // Set POWER_OFF high [issue37308]
	mxc_gpio_direction(GPIO_POWER_OFF, MXC_GPIO_DIRECTION_OUT);


/* EXPANSION_EN */
	mxc_request_iomux(MX51_PIN_CSPI1_RDY , IOMUX_CONFIG_ALT3); /* GPIO4_26 */
	mxc_iomux_set_pad(MX51_PIN_CSPI1_RDY , PAD_CTL_DRV_HIGH | PAD_CTL_PKE_NONE);
	mxc_gpio_set(GPIO_EXPANSION_EN, 0);
	mxc_gpio_direction(GPIO_EXPANSION_EN, MXC_GPIO_DIRECTION_OUT);


/* KEYBOARD_BACKLIGHT_EN */
	mxc_request_iomux(MX51_PIN_OWIRE_LINE, IOMUX_CONFIG_ALT3); /* GPIO1_24 */
	mxc_iomux_set_pad(MX51_PIN_OWIRE_LINE, 0x82);
	mxc_gpio_set(GPIO_KEY_BGLIGHT_EN, 0);
	mxc_gpio_direction(GPIO_KEY_BGLIGHT_EN, MXC_GPIO_DIRECTION_OUT);

/* DAB_PWM */
#ifdef CONFIG_LCD
	// configure as GPIO and set to low; PWM will be set together with
	// DAB_LIGHT_EN during command "lcdbl on"
	mxc_request_iomux(MX51_PIN_GPIO1_2, IOMUX_CONFIG_ALT0);
	mxc_gpio_set(GPIO_DAB_PWM, 0);
	mxc_gpio_direction(GPIO_DAB_PWM, MXC_GPIO_DIRECTION_OUT);
#else
	// no display - configure for PWM, brightness dont care
	mxc_request_iomux(MX51_PIN_GPIO1_2, IOMUX_CONFIG_ALT1);
#endif

/* prepare GPIO_SPI_CS1n_FLASH pin */
	mxc_gpio_set(GPIO_SPI_CS1n_FLASH, 1);
	mxc_gpio_direction(GPIO_SPI_CS1n_FLASH, MXC_GPIO_DIRECTION_OUT);

	WATCHDOG_RESET();
}

static void setup_fec(void)
{
	/*FEC_MDIO*/
	mxc_request_iomux(MX51_PIN_EIM_EB2 , IOMUX_CONFIG_ALT3);
	mxc_iomux_set_pad(MX51_PIN_EIM_EB2 , 0x1FD);

	/*FEC_MDC*/
	mxc_request_iomux(MX51_PIN_NANDF_CS3, IOMUX_CONFIG_ALT2);
	mxc_iomux_set_pad(MX51_PIN_NANDF_CS3, 0x2004);

	/* FEC RDATA[3] */
	mxc_request_iomux(MX51_PIN_EIM_CS3, IOMUX_CONFIG_ALT3);
	mxc_iomux_set_pad(MX51_PIN_EIM_CS3, 0x180);

	/* FEC RDATA[2] */
	mxc_request_iomux(MX51_PIN_EIM_CS2, IOMUX_CONFIG_ALT3);
	mxc_iomux_set_pad(MX51_PIN_EIM_CS2, 0x180);

	/* FEC RDATA[1] */
	mxc_request_iomux(MX51_PIN_EIM_EB3, IOMUX_CONFIG_ALT3);
	mxc_iomux_set_pad(MX51_PIN_EIM_EB3, 0x180);

	/* FEC RDATA[0] */
	mxc_request_iomux(MX51_PIN_NANDF_D9, IOMUX_CONFIG_ALT2);
	mxc_iomux_set_pad(MX51_PIN_NANDF_D9, 0x2180);

	/* FEC TDATA[3] */
	mxc_request_iomux(MX51_PIN_NANDF_CS6, IOMUX_CONFIG_ALT2);
	mxc_iomux_set_pad(MX51_PIN_NANDF_CS6, 0x2004);

	/* FEC TDATA[2] */
	mxc_request_iomux(MX51_PIN_NANDF_CS5, IOMUX_CONFIG_ALT2);
	mxc_iomux_set_pad(MX51_PIN_NANDF_CS5, 0x2004);

	/* FEC TDATA[1] */
	mxc_request_iomux(MX51_PIN_NANDF_CS4, IOMUX_CONFIG_ALT2);
	mxc_iomux_set_pad(MX51_PIN_NANDF_CS4, 0x2004);

	/* FEC TDATA[0] */
	mxc_request_iomux(MX51_PIN_NANDF_D8, IOMUX_CONFIG_ALT2);
	mxc_iomux_set_pad(MX51_PIN_NANDF_D8, 0x2004);

	/* FEC TX_EN */
	mxc_request_iomux(MX51_PIN_NANDF_CS7, IOMUX_CONFIG_ALT1);
	mxc_iomux_set_pad(MX51_PIN_NANDF_CS7, 0x2004);

	/* FEC TX_ER */
	mxc_request_iomux(MX51_PIN_NANDF_CS2, IOMUX_CONFIG_ALT2);
	mxc_iomux_set_pad(MX51_PIN_NANDF_CS2, 0x2004);

	/* FEC TX_CLK */
	mxc_request_iomux(MX51_PIN_NANDF_RDY_INT, IOMUX_CONFIG_ALT1);
	mxc_iomux_set_pad(MX51_PIN_NANDF_RDY_INT, 0x2180);

	/* FEC TX_COL */
	mxc_request_iomux(MX51_PIN_NANDF_RB2, IOMUX_CONFIG_ALT1);
	mxc_iomux_set_pad(MX51_PIN_NANDF_RB2, 0x2180);

	/* FEC RX_CLK */
	mxc_request_iomux(MX51_PIN_NANDF_RB3, IOMUX_CONFIG_ALT1);
	mxc_iomux_set_pad(MX51_PIN_NANDF_RB3, 0x2180);

	/* FEC RX_CRS */
	mxc_request_iomux(MX51_PIN_EIM_CS5, IOMUX_CONFIG_ALT3);
	mxc_iomux_set_pad(MX51_PIN_EIM_CS5, 0x180);

	/* FEC RX_ER */
	mxc_request_iomux(MX51_PIN_EIM_CS4, IOMUX_CONFIG_ALT3);
	mxc_iomux_set_pad(MX51_PIN_EIM_CS4, 0x180);

	/* FEC RX_DV */
	mxc_request_iomux(MX51_PIN_NANDF_D11, IOMUX_CONFIG_ALT2);
	mxc_iomux_set_pad(MX51_PIN_NANDF_D11, 0x2180);
}

struct fsl_esdhc_cfg esdhc_cfg[1] = {
        {MMC_SDHC1_BASE_ADDR, 1},
};

int get_mmc_getcd(u8 *cd, struct mmc *mmc)
{
	struct fsl_esdhc_cfg *cfg = (struct fsl_esdhc_cfg *)mmc->priv;

	if (cfg->esdhc_base == MMC_SDHC1_BASE_ADDR)
                *cd = mxc_gpio_get(GPIO_SD_CD);
	else
		*cd = 0;

	return 0;
}

void setup_mmc()
{
        mxc_request_iomux(MX51_PIN_SD1_CMD, IOMUX_CONFIG_ALT0 | IOMUX_CONFIG_SION);
        mxc_request_iomux(MX51_PIN_SD1_CLK, IOMUX_CONFIG_ALT0 | IOMUX_CONFIG_SION);
        mxc_request_iomux(MX51_PIN_SD1_DATA0, IOMUX_CONFIG_ALT0 | IOMUX_CONFIG_SION);
        mxc_request_iomux(MX51_PIN_SD1_DATA1, IOMUX_CONFIG_ALT0 | IOMUX_CONFIG_SION);
        mxc_request_iomux(MX51_PIN_SD1_DATA2, IOMUX_CONFIG_ALT0 | IOMUX_CONFIG_SION);
        mxc_request_iomux(MX51_PIN_SD1_DATA3, IOMUX_CONFIG_ALT0 | IOMUX_CONFIG_SION);
        mxc_iomux_set_pad(MX51_PIN_SD1_CMD, PAD_CTL_DRV_MAX | PAD_CTL_DRV_VOT_HIGH | PAD_CTL_HYS_ENABLE | PAD_CTL_47K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_SRE_FAST);
        mxc_iomux_set_pad(MX51_PIN_SD1_CLK, PAD_CTL_DRV_MAX | PAD_CTL_DRV_VOT_HIGH | PAD_CTL_HYS_NONE | PAD_CTL_47K_PU | PAD_CTL_PUE_PULL |     PAD_CTL_PKE_ENABLE | PAD_CTL_SRE_FAST);
        mxc_iomux_set_pad(MX51_PIN_SD1_DATA0, PAD_CTL_DRV_MAX | PAD_CTL_DRV_VOT_HIGH | PAD_CTL_HYS_ENABLE | PAD_CTL_47K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_SRE_FAST);
        mxc_iomux_set_pad(MX51_PIN_SD1_DATA1, PAD_CTL_DRV_MAX | PAD_CTL_DRV_VOT_HIGH | PAD_CTL_HYS_ENABLE | PAD_CTL_47K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_SRE_FAST);
        mxc_iomux_set_pad(MX51_PIN_SD1_DATA2, PAD_CTL_DRV_MAX | PAD_CTL_DRV_VOT_HIGH | PAD_CTL_HYS_ENABLE | PAD_CTL_47K_PU | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_SRE_FAST);
        mxc_iomux_set_pad(MX51_PIN_SD1_DATA3, PAD_CTL_DRV_MAX | PAD_CTL_DRV_VOT_HIGH | PAD_CTL_HYS_ENABLE | PAD_CTL_100K_PD | PAD_CTL_PUE_PULL | PAD_CTL_PKE_ENABLE | PAD_CTL_SRE_FAST);
        mxc_request_iomux(MX51_PIN_GPIO1_0, IOMUX_CONFIG_ALT0 | IOMUX_CONFIG_SION);
        mxc_iomux_set_pad(MX51_PIN_GPIO1_0, PAD_CTL_HYS_ENABLE);
        mxc_request_iomux(MX51_PIN_GPIO1_1,     IOMUX_CONFIG_ALT0 | IOMUX_CONFIG_SION);
        mxc_iomux_set_pad(MX51_PIN_GPIO1_1,     PAD_CTL_HYS_ENABLE);
}

int board_mmc_init(bd_t *bis)
{
#ifdef CONFIG_FSL_ESDHC
	return (fsl_esdhc_initialize(bis, &esdhc_cfg[0]));
#else
	return 0;
#endif
}

#ifdef CONFIG_LCD
static void backlight(int on)
{
	mxc_gpio_set(GPIO_DAB_PWM, on);
	mxc_gpio_set(GPIO_DAB_BACKLIGHT_EN, on);
}

u8 parse_videomode(struct fb_videomode * vmode)
{
	char *env_videomode;
	//char *next_param;
	u32 *mode = (u32 *)vmode;
	u32 param_count = 0;

	mode++; // skip name pointer
	env_videomode = getenv("videomode");
	if (env_videomode != NULL)
	{
		do
		{
			*mode++ = simple_strtoul(env_videomode, &env_videomode, 0);
		} while ((param_count++ < 13) && (*env_videomode++ == ','));
		
		if (param_count < 13)
		{
			puts("Video mode has not enough parameters.\n");
			return 0;
		}
		else
		{
			return 1;
		}
	}
	else
	{
		puts("No video mode set. Not enabling display.\n");
		return 0;
	}
}

void lcd_enable(void)
{
	if ((parse_videomode(&videomode)) && (mx51_fb_init(&videomode)))
	{
		puts("LCD cannot be configured\n");
	}
}
#endif

#ifdef CONFIG_CONSOLE_BANNER

int display_console_banner (void)
{

  puts("\n");
  puts("                 _____________________\n");
  puts("   .      _ ____/                     \\____ _\n");
  puts("       ____\\        V i S i O N �        //_____\n");
  puts(". .:...\\        __/\\            __/\\            /_____ .xxx.\n");
  puts("   :            \\   \\           \\   \\                  \"  X/\n");
  puts("     /\\    ___  _\\__/ ______    _\\__/ ________   _       X/\n");
  puts("    /  \\  /   \\/    //  ___/_  /    /_\\   _   \\_/ \\\\   .XX.X\n");
  puts("  _/    \\/    /    //____    \\/    //     /    /   \\\\/\n");
  puts("  \\     /    /    //    /    /    //     /    /     \\    /\n");
  puts("   \\   /    /    //    /    /    //     /    /     \\    /\n");
  puts("    \\  ____/    //___  ____/    // \\___  ___/\\____/ \\  /\n");
  puts("     \\/   /_____/    \\/   /_____/      \\/            \\/\n");
  puts(". .:.. __                                      ______ ..:. .\n");
  puts("   :    /____________              ____________\\        :\n");
  puts("   .                _\\\\__________//_                    .\n");
  puts("\n");

	return 0;
}


#endif



int board_init(void)
{
#ifndef CONFIG_ICACHE_OFF
        icache_enable();
        //dcache_enable();
#endif

#ifdef CONFIG_SILENT_CONSOLE
	gd->flags |= GD_FLG_SILENT;
#endif

	init_drive_strength();

	/* Setup debug led */
	mxc_request_iomux(MX51_PIN_GPIO1_6, IOMUX_CONFIG_ALT0);
	mxc_iomux_set_pad(MX51_PIN_GPIO1_6,	PAD_CTL_DRV_MAX | PAD_CTL_SRE_FAST);
	mxc_gpio_set(GPIO_DEBUG_LED, 0);
	mxc_gpio_direction(GPIO_DEBUG_LED, MXC_GPIO_DIRECTION_OUT);

	/* wait a little while to give the pll time to settle */
	sdelay(100000);

	gd->bd->bi_arch_number = MACH_TYPE_TTC_VISION2;	/* board id for linux */
	/* address of boot parameters */
	gd->bd->bi_boot_params = PHYS_SDRAM_1 + 0x100;

	setup_weim();
	
	setup_gpios();
	
	setup_uart();
		
	setup_fec();

	setup_mmc();
	
	spi_io_init();

	return 0;
}

int board_late_init(void)
{
	power_init_mx51();

	return 0;
}

int checkboard(void)
{	
	u32 system_rev = get_cpu_rev();
	u32 cause;
	struct src *src_regs = (struct src *)SRC_BASE_ADDR;

	puts("Board: TTControl Vision� CPU V");

	switch (system_rev & 0xff) {
	case CHIP_REV_3_0:
		puts("3.0 [");
		break;
	case CHIP_REV_2_5:
		puts("2.5 [");
		break;
	case CHIP_REV_2_0:
		puts("2.0 [");
		break;
	case CHIP_REV_1_1:
		puts("1.1 [");
		break;
	case CHIP_REV_1_0:
	default:
		puts("1.0 [");
		break;
	}

	cause = src_regs->srsr;
	switch (cause) {
	case 0x0001:
		puts("POR");
		break;
	case 0x0009:
		puts("RST");
		break;
	case 0x0010:
	case 0x0011:
		puts("WDOG");
		break;
	default:
		printf("unknown 0x%x", cause);
	}
	puts("]\n");
	
	return 0;
}

int do_vision_outport(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	if (argc < 4)
		return cmd_usage(cmdtp);
	
	mxc_gpio_set(GPIO_OUTPORT0, (argv[1] == '0') ? 0 : 1);
	mxc_gpio_set(GPIO_OUTPORT1, (argv[2] == '0') ? 0 : 1);
	mxc_gpio_set(GPIO_OUTPORT2, (argv[3] == '0') ? 0 : 1);

	return 0;
}

U_BOOT_CMD(
	outport, CONFIG_SYS_MAXARGS, 1, do_vision_outport,
	"Vision2 Digital Outputs",
	"outport <out1> <out2> <out3>\n"
);

#ifdef CONFIG_LCD
int do_vision_lcd(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	int on;

	if (argc < 2)
		return cmd_usage(cmdtp);

	on = (strcmp(argv[1], "on") == 0);
	backlight(on);

	return 0;
}

U_BOOT_CMD(
	lcdbl,	CONFIG_SYS_MAXARGS, 1, do_vision_lcd,
	"Vision2 Backlight",
	"lcdbl [on|off]\n"
);
#endif

