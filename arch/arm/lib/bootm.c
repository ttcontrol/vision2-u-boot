/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * Copyright (C) 2001  Erik Mouw (J.A.K.Mouw@its.tudelft.nl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	 USA
 *
 */

#include <common.h>
#include <command.h>
#include <image.h>
#include <u-boot/zlib.h>
#include <asm/byteorder.h>

#include <mxc_gpio.h>

#include <linux/fb.h>

DECLARE_GLOBAL_DATA_PTR;

#if defined (CONFIG_SETUP_MEMORY_TAGS) || \
    defined (CONFIG_CMDLINE_TAG) || \
    defined (CONFIG_INITRD_TAG) || \
    defined (CONFIG_SERIAL_TAG) || \
    defined (CONFIG_VIDEOFB_TAG) || \
    defined (CONFIG_REVISION_TAG)
static void setup_start_tag (bd_t *bd);

# ifdef CONFIG_SETUP_MEMORY_TAGS
static void setup_memory_tags (bd_t *bd);
# endif
static void setup_commandline_tag (bd_t *bd, char *commandline);

# ifdef CONFIG_INITRD_TAG
static void setup_initrd_tag (bd_t *bd, ulong initrd_start,
			      ulong initrd_end);
# endif

#ifdef CONFIG_VIDEOFB_TAG
static void setup_videofb_tag(void);
#endif

#ifdef CONFIG_VISION2_DEVICEINFO_TAG
static void setup_vision2_deviceinfo_tag (void);
#endif

#ifdef CONFIG_VISION2_BOOTSPLASH_TAG
static void setup_vision2_bootsplash_tag (void);
#endif

static void setup_end_tag (bd_t *bd);

static struct tag *params;
#endif /* CONFIG_SETUP_MEMORY_TAGS || CONFIG_CMDLINE_TAG || CONFIG_INITRD_TAG */

int do_bootm_linux(int flag, int argc, char * const argv[], bootm_headers_t *images)
{
	bd_t	*bd = gd->bd;
	char	*s;
	int	machid = bd->bi_arch_number;
	void	(*theKernel)(int zero, int arch, uint params);

#ifdef CONFIG_CMDLINE_TAG
	char *commandline = getenv ("bootargs");
#endif

	if ((flag != 0) && (flag != BOOTM_STATE_OS_GO))
		return 1;

	theKernel = (void (*)(int, int, uint))images->ep;

	s = getenv ("machid");
	if (s) {
		machid = simple_strtoul (s, NULL, 16);
		printf ("Using machid 0x%x from environment\n", machid);
	}

	show_boot_progress (15);

	debug ("## Transferring control to Linux (at address %08lx) ...\n",
	       (ulong) theKernel);

#if defined (CONFIG_SETUP_MEMORY_TAGS) || \
    defined (CONFIG_CMDLINE_TAG) || \
    defined (CONFIG_INITRD_TAG) || \
    defined (CONFIG_SERIAL_TAG) || \
    defined (CONFIG_REVISION_TAG)
	setup_start_tag (bd);
#ifdef CONFIG_SERIAL_TAG
	setup_serial_tag (&params);
#endif
#ifdef CONFIG_REVISION_TAG
	setup_revision_tag (&params);
#endif
#ifdef CONFIG_SETUP_MEMORY_TAGS
	setup_memory_tags (bd);
#endif
#ifdef CONFIG_CMDLINE_TAG
	setup_commandline_tag (bd, commandline);
#endif
#ifdef CONFIG_VIDEOFB_TAG
	setup_videofb_tag ();
#endif
#ifdef CONFIG_VISION2_DEVICEINFO_TAG
	setup_vision2_deviceinfo_tag ();
#endif
#ifdef CONFIG_VISION2_BOOTSPLASH_TAG
	setup_vision2_bootsplash_tag ();
#endif
#ifdef CONFIG_INITRD_TAG
	if (images->rd_start && images->rd_end)
		setup_initrd_tag (bd, images->rd_start, images->rd_end);
#endif
	setup_end_tag (bd);
#endif

	/* we assume that the kernel is in place */
	printf ("\nStarting kernel ...\n\n");

#ifdef CONFIG_USB_DEVICE
	{
		extern void udc_disconnect (void);
		udc_disconnect ();
	}
#endif

	cleanup_before_linux ();

	theKernel (0, machid, bd->bi_boot_params);
	/* does not return */

	return 1;
}


#if defined (CONFIG_SETUP_MEMORY_TAGS) || \
    defined (CONFIG_CMDLINE_TAG) || \
    defined (CONFIG_INITRD_TAG) || \
    defined (CONFIG_SERIAL_TAG) || \
    defined (CONFIG_VIDEOFB_TAG) || \
    defined (CONFIG_REVISION_TAG)
static void setup_start_tag (bd_t *bd)
{
	params = (struct tag *) bd->bi_boot_params;

	params->hdr.tag = ATAG_CORE;
	params->hdr.size = tag_size (tag_core);

	params->u.core.flags = 0;
	params->u.core.pagesize = 0;
	params->u.core.rootdev = 0;

	params = tag_next (params);
}


#ifdef CONFIG_SETUP_MEMORY_TAGS
static void setup_memory_tags (bd_t *bd)
{
	int i;

	for (i = 0; i < CONFIG_NR_DRAM_BANKS; i++) {
		params->hdr.tag = ATAG_MEM;
		params->hdr.size = tag_size (tag_mem32);

		params->u.mem.start = bd->bi_dram[i].start;
		params->u.mem.size = bd->bi_dram[i].size;

		params = tag_next (params);
	}
}
#endif /* CONFIG_SETUP_MEMORY_TAGS */


static void setup_commandline_tag (bd_t *bd, char *commandline)
{
	char *p;

	if (!commandline)
		return;

	/* eat leading white space */
	for (p = commandline; *p == ' '; p++);

	/* skip non-existent command lines so the kernel will still
	 * use its default command line.
	 */
	if (*p == '\0')
		return;

	strcpy (params->u.cmdline.cmdline, p);
#ifdef CONFIG_SWITCH_UART_BY_VBAT_EN
	if (gd->uart_base_address == 0x07000C000)
	{
		strcat (params->u.cmdline.cmdline, " console=ttymxc2,115200");
	}
	else
	{
		strcat (params->u.cmdline.cmdline, " console=ttymxc0,115200");
	}
#else
	strcat (params->u.cmdline.cmdline, " console=ttymxc2,115200");
#endif

	params->hdr.tag = ATAG_CMDLINE;
	params->hdr.size =
		(sizeof (struct tag_header) + strlen (params->u.cmdline.cmdline) + 1 + 4) >> 2;

	params = tag_next (params);
}


#ifdef CONFIG_INITRD_TAG
static void setup_initrd_tag (bd_t *bd, ulong initrd_start, ulong initrd_end)
{
	/* an ATAG_INITRD node tells the kernel where the compressed
	 * ramdisk can be found. ATAG_RDIMG is a better name, actually.
	 */
	params->hdr.tag = ATAG_INITRD2;
	params->hdr.size = tag_size (tag_initrd);

	params->u.initrd.start = initrd_start;
	params->u.initrd.size = initrd_end - initrd_start;

	params = tag_next (params);
}
#endif /* CONFIG_INITRD_TAG */

#ifdef CONFIG_SERIAL_TAG
void setup_serial_tag (struct tag **tmp)
{
	struct tag *params = *tmp;
	struct tag_serialnr serialnr;
	void get_board_serial(struct tag_serialnr *serialnr);

	get_board_serial(&serialnr);
	params->hdr.tag = ATAG_SERIAL;
	params->hdr.size = tag_size (tag_serialnr);
	params->u.serialnr.low = serialnr.low;
	params->u.serialnr.high= serialnr.high;
	params = tag_next (params);
	*tmp = params;
}
#endif

#ifdef CONFIG_REVISION_TAG
void setup_revision_tag(struct tag **in_params)
{
	u32 rev = 0;
	u32 get_board_rev(void);

	rev = get_board_rev();
	params->hdr.tag = ATAG_REVISION;
	params->hdr.size = tag_size (tag_revision);
	params->u.revision.rev = rev;
	params = tag_next (params);
}
#endif  /* CONFIG_REVISION_TAG */

#ifdef CONFIG_VIDEOFB_TAG
extern struct fb_videomode videomode;
void setup_videofb_tag()
{
	params->hdr.tag = ATAG_VIDEOFB;
	params->hdr.size = tag_size (tag_fbvideomode);
	params->u.fbvideomode.refresh = videomode.refresh;
	params->u.fbvideomode.xres = videomode.xres;
	params->u.fbvideomode.yres = videomode.yres;
	params->u.fbvideomode.pixclock = videomode.pixclock;
	params->u.fbvideomode.left_margin = videomode.left_margin;
	params->u.fbvideomode.right_margin = videomode.right_margin;
	params->u.fbvideomode.upper_margin = videomode.upper_margin;
	params->u.fbvideomode.lower_margin = videomode.lower_margin;
	params->u.fbvideomode.hsync_len = videomode.hsync_len;
	params->u.fbvideomode.vsync_len = videomode.vsync_len;
	params->u.fbvideomode.sync = videomode.sync;
	params->u.fbvideomode.vmode = videomode.vmode;
	params->u.fbvideomode.flag = videomode.flag;
	params = tag_next (params);
}
#endif

#ifdef CONFIG_VISION2_DEVICEINFO_TAG
void setup_vision2_deviceinfo_tag()
{
	char * s;

	params->hdr.tag = ATAG_VISION2_DEVICEINFO;
	params->hdr.size = tag_size (tag_vision2_deviceinfo);
	s = getenv("deviceid");
	if (s)
	{
		params->u.vision2_deviceinfo.id = (u16)(simple_strtoul(s, NULL, 16) & 0xFFFF);
	}
	else
	{
		params->u.vision2_deviceinfo.id = 0;
	}

	s = getenv("serial#");
	if (s)
	{
		strncpy(params->u.vision2_deviceinfo.serial_board, s, 32);
	}
	else
	{
		memset(params->u.vision2_deviceinfo.serial_board, 0, 32);
	}

	s = getenv("box_serial");
	if (s)
	{
		strncpy(params->u.vision2_deviceinfo.serial_device, s, 32);
	}
	else
	{
		memset(params->u.vision2_deviceinfo.serial_device, 0, 32);
	}

	s = getenv("sw_release");
	if (s)
	{
		params->u.vision2_deviceinfo.sw_release = simple_strtoul(s, NULL, 16);
	}
	else
	{
		params->u.vision2_deviceinfo.sw_release = 0xFFFFFFFF;
	}

	s = getenv("hw_revision");
	if (s)
	{
		params->u.vision2_deviceinfo.hw_revision = simple_strtoul(s, NULL, 10);
	}
	else
	{
		params->u.vision2_deviceinfo.hw_revision = 0xFFFFFFFF;
	}

	if (mxc_gpio_get(GPIO_VBAT_EN_KL30))
	{
		params->u.vision2_deviceinfo.mode = 1;
	}
	else
	{
		params->u.vision2_deviceinfo.mode = 0;
	}

	printf("device id: 0x%x\n", params->u.vision2_deviceinfo.id);
	printf("mode: %d\n", params->u.vision2_deviceinfo.mode);
	printf("sw_release: 0x%x\n", params->u.vision2_deviceinfo.sw_release);
	printf("board_serial: %s\n", params->u.vision2_deviceinfo.serial_board);
	printf("device_serial: %s\n", params->u.vision2_deviceinfo.serial_device);

	params = tag_next (params);
}
#endif

#ifdef CONFIG_VISION2_BOOTSPLASH_TAG
extern u32 vision2_framebuffer_address(void);
extern u32 vision2_framebuffer_length(void);
void setup_vision2_bootsplash_tag()
{
	params->hdr.tag = ATAG_VISION2_BOOTSPLASH;
	params->hdr.size = tag_size (tag_vision2_bootsplash);

	params->u.vision2_bootsplash.start  = vision2_framebuffer_address ();
	params->u.vision2_bootsplash.length = vision2_framebuffer_length ();
	printf("bootsplash: start=0x%X, length=0x%X\n"
	      , params->u.vision2_bootsplash.start
	      , params->u.vision2_bootsplash.length
	      );
	params = tag_next (params);
}
#endif

static void setup_end_tag (bd_t *bd)
{
	params->hdr.tag = ATAG_NONE;
	params->hdr.size = 0;
}

#endif /* CONFIG_SETUP_MEMORY_TAGS || CONFIG_CMDLINE_TAG || CONFIG_INITRD_TAG */
