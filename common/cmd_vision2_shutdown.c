/*
 * Command for accessing DataFlash.
 *
 * Copyright (C) 2008 Atmel Corporation
 */
#include <common.h>
#include <asm/io.h>
#include <asm/arch/imx-regs.h>
#include <fsl_pmic.h>
#include <mc13892.h>
#include <mxc_gpio.h>
#include <asm/arch/iomux.h>


static int do_shutdown(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	unsigned int val; 

	/* do not need any arguments */
	if (argc > 1)
		goto usage;
	
	/* shut off if KL15 is off */
	mxc_gpio_set(GPIO_POWER_OFF, 1);
//	writel((1<<27), GPIO3_BASE_ADDR + 0x0); 

	/* reset if KL 15 is ON */
	val = pmic_reg_read(REG_POWER_CTL1);
	val &= ~(0xF00); /* PCCOUNT */
	val |= (0xF000); /* PCMAXCNT */
	pmic_reg_write(REG_POWER_CTL1, val);
	
	val = pmic_reg_read(REG_POWER_CTL2);
	val |= (1<<12); /* WDIRESET */
	pmic_reg_write(REG_POWER_CTL2, val);
	
	// Set GPIO1_4 to low to reset the system with PMIC
	mxc_gpio_set(GPIO_WATCHDOG_RESETn, 0);
	mxc_gpio_direction(GPIO_WATCHDOG_RESETn, MXC_GPIO_DIRECTION_OUT);

	/* WATCHDOG RESET */
	mxc_iomux_set_pad(MX51_PIN_GPIO1_4 , 0x8f);
	mxc_request_iomux(MX51_PIN_GPIO1_4 , IOMUX_CONFIG_ALT0); /* GPIO1_4 */

	return 0;

usage:
	cmd_usage(cmdtp);
	return 1;
}

U_BOOT_CMD(
	vision2_shutdown,	1,	1,	do_shutdown,
	"System shutdown",
	"Use shutdown to power off your system"
	);
