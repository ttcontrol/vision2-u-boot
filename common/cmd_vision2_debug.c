/*
 * Commands for for Vision� Debugging (Voltages, PMIC).
 *
 * Copyright (C) 
 */
#include <common.h>
#include <asm/io.h>
#include <asm/arch/imx-regs.h>
#include <fsl_pmic.h>
#include <mc13892.h>

static int do_vision2_debug(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	/* do not need any arguments */
	if (argc != 3)
		goto usage;
		
	if (strncmp(argv[1], "3v15", 4) == 0) 
	{
		unsigned int val;

	/* Set VGEN3 to 1.8V, VCAM to 3.0V */
		val = pmic_reg_read(REG_MODE_0);
		
		if (strncmp(argv[2], "on", 2) == 0) 
			val |= 0x1000; /* VGEN2EN */
		else
			val &= ~0x1000;

		pmic_reg_write(REG_MODE_0, val);
			
	}
	else
		if (strncmp(argv[1], "cam", 3) == 0) 
		{
			unsigned int val;

			if (strncmp(argv[2], "on", 2) == 0)
				mxc_gpio_set(GPIO_CAM_EN, 1); // set to on
			else
				mxc_gpio_set(GPIO_CAM_EN, 0); // set to off

		}

	return 0;

usage:
	cmd_usage(cmdtp);
	return 1;
}


U_BOOT_CMD(
	vision2_debug,	3,	0,	do_vision2_debug,
	"vision2 debug",
	"Use vision2_debug to debug your vision2 system"
	);

int do_pmic_dump(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	unsigned int i;
	unsigned int val;

	if (!(mxc_gpio_get(GPIO_VBAT_EN_KL30)))
	{
		printf("\nVision2 not in debug mode\n");
		return 1;
	}

	for (i = 0; i < 64; i++)
	{
		val = pmic_reg_read(i);
		if (!(i % 16))
		{
			printf("\n%02d: ", i);
		}
		printf(" %08x", val);
	}
	printf("\n");
}

U_BOOT_CMD(
	vision2_pmicdump, CONFIG_SYS_MAXARGS, 1, do_pmic_dump,
	"Dump PMIC settings",
	""
);